$(document).ready(function(){

    // Modals Toggles
    var optionText;

    $("#coffee").click(function(){

        $('#coffeePage').modal();
        
        $('#coffeeDrinks').on('change', function () {
            optionText = $("#coffeeDrinks option:selected").text();
            
            $('#cancelCoffee').click(function(){
                $('#drinkOutput').val("");
            });

            $('#acceptCoffee').click(function(){
                $('#drinkOutput').val(optionText);
            });

        });

    });
    
    $("#tea").click(function(){
        $('#teaPage').modal();
        
        $('#teaDrinks').on('change', function () {
            optionText = $("#teaDrinks option:selected").text();
            
            $('#cancelTea').click(function(){
                $('#drinkOutput').val("");
            });

            $('#acceptTea').click(function(){
                $('#drinkOutput').val(optionText);
            });

        });
    });

    $("#shake").click(function(){
        $('#shakesPage').modal();
        
        $('#shakesDrinks').on('change', function () {
            optionText = $("#shakesDrinks option:selected").text();
            
            $('#cancelShakes').click(function(){
                $('#drinkOutput').val("");
            });

            $('#acceptShakes').click(function(){
                $('#drinkOutput').val(optionText);
            });

        });
    });

    // Fix for readonly required
    $('.readonly').keydown(function(e){
        e.preventDefault();
    });

});