# Coffee

    1. Café au Lait
        - (image file)[https://i.ytimg.com/vi/iZNriRSj1ZA/maxresdefault.jpg]
        - French for "coffee with milk"; the most basic type of coffee that is found around the world. No one could resist the basic Café au Lait, and it is also probably found in most 3-in-1 packs. In addition, Café au lait is a popular drink in New Orleans, available at coffee shops like Café du Monde and Morning Call Coffee Stand, where it is made with milk and coffee mixed with chicory, giving it a strong, bitter taste.Unlike the European café style, a New Orleans-style café au lait is made with scalded milk (milk warmed over heat to just below boiling), rather than with steamed milk. (Wikipedia)

    2. Mocha
        - (image file)[https://gatherforbread.com/wp-content/uploads/2014/10/Dark-Chocolate-Mocha-Square.jpg]
        - A caffè mocha, also called mocaccino, is a chocolate-flavoured variant of a caffè latte. Other commonly used spellings are mochaccino and also mochachino. The name is derived from the city of Mocha, Yemen, which was one of the centers of early coffee trade. (Wikipedia) It is also a very famous flavour wherein it is considered to be a mix of the two most famous and caffeinated beans popular around the world: coffee, and cacao. For a sense of confusion, it finds itself in an equilibrium of flavours. The strong taste of coffee, and the bassy undertone of the cacao, they are like the yin and yang of any coffee blends. 
    
    3. Mazagran
        - (image file)[https://lirp-cdn.multiscreensite.com/2737510f/dms3rep/multi/opt/mazagran002-1920w.jpg]
        - Mazagran is a cold, sweetened coffee drink that originated in Algeria. Portuguese versions may use espresso, lemon, mint and rum, and Austrian versions are served with an ice cube and include rum. Sometimes a fast version is achieved by pouring a previously sweetened espresso in a cup with ice cubes and a slice of lemon. Mazagran has been described as "the original iced coffee". (Wikipedia)

# Tea

    1. Black Tea
        - (image file)[https://www.nutritionadvance.com/wp-content/uploads/2018/09/types-of-black-tea.jpg]
        - Black tea is a type of tea that is more oxidized than oolong, yellow, white and green teas. Black tea is generally stronger in flavor than other teas. All four types are made from leaves of the shrub (or small tree) Camellia sinensis. Two principal varieties of the species are used – the small-leaved Chinese variety plant (C. sinensis var. sinensis), used for most other types of teas, and the large-leaved Assamese plant (C. sinensis var. assamica), which was traditionally mainly used for black tea, although in recent years some green and white teas have been produced. In China, where black tea was developed, the beverage is called 紅茶 "red tea", due to the color of the oxidized leaves when processed appropriately. (Wikipedia)

    2. Matcha Tea
        - (image file)[https://cdn.drweil.com/wp-content/uploads/2016/12/diet-nutrition_nutrition_discover-matcha-tea_2716x1810_000055981922.jpg]
        - Matcha is finely ground powder of specially grown and processed green tea leaves, traditionally consumed in East Asia. The green tea plants used for matcha are shade-grown for three to four weeks before harvest; the stems and veins are removed during processing. During shaded growth, the plant Camellia sinensis produces more theanine and caffeine. The powdered form of matcha is consumed differently from tea leaves or tea bags, as it is suspended in a liquid, typically water or milk.

    3. Chai Tea
        - (image file)[]
        - Masala Chai, otherwise known as Chai Tea, is a tea beverage made by boiling black tea in milk and water with a mixture of aromatic herbs and spices. Originating in India, the beverage has gained worldwide popularity, becoming a feature in many coffee and tea houses. Although traditionally prepared as a decoction of green cardamom pods, cinnamon sticks, ground cloves, ground ginger, and black peppercorn together with black tea leaves, retail versions include tea bags for infusion, instant powdered mixtures, and concentrates. (Wikipedia)

# Shakes
    
    1. Watermelon Shake
        - (image file)[https://lilluna.com/wp-content/uploads/2018/06/watermelon-smoothie-resize-4-683x1024.jpg]
        - Now you may be wondering, why did I include 2 kinds of the same fruit shake? Well, these are entirely different fruits prepared the same way, but in taste, they are completely different. Watermelon shake has that distinct sweet flavour that comes from the watermelon itself. The flavour of watermelon is really well-known and can really represent a refreshing scent and taste that makes you feel like you're on a holiday, and it can be served up as a shake or you can just enjoy the juicy fruit on your own while sitting on the beach, staring at the sun or whatever. This is one of the shakes that deserve an umbrella.

    2. Mango Shake
        - (image file)[http://www.blog.sagmart.com/wp-content/uploads/2015/05/Mango-Shake.png]
        - Again, watermelon shake and mango shake are similar fruit shakes, but the only difference with them is on how they taste. While watermelon is more on that very notable sweet flavour, the mango also has that sort of sweet and soury flavour that is more distinct than the watermelon. Now don't get me wrong, they both can be very sweet, but the sweetness of the watermelon is unprecedented. Mango has a zesty sour taste to it, making it unique. It is also more tropical than the watermelon, since besides being the national fruit of the Philippines, it is mostly found on tropical countries, and its really best found when its ripe.

    3. Milkshake
        - (image file)[https://www.pointfranchise.co.uk/images/zoom/articles/milkshake-shop-franchises.png]
        - A milkshake, or simply shake, is a drink that is traditionally made by blending cow's milk, ice cream, and flavorings or sweeteners such as butterscotch, caramel sauce, chocolate syrup, fruit syrup, or whole fruit into a thick, sweet, cold mixture. It may also be made using other types of milk such as almond milk, coconut milk, or hemp milk. (Wikipedia) It is simply blending vanilla ice cream with milk, in other words. Fruits optional.