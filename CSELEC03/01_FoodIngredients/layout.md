# DONE 1. Kare-Kare

    ## Description
        - Kare-kare is a Philippine stew complemented with a thick savory peanut sauce. It is made from a variation base of stewed oxtail, pork hocks, calves feet, pig feet, beef stew meat, and occasionally offal or tripe. Kare-kare can also be made with seafood or vegetables. (Wikipedia)

    [Recipe Source](https://panlasangpinoy.com/kare-kare-recipe/)

    ## Ingredients
        
        3       lbs         oxtail (cut in 2 inch slices you an also use tripe or beef slices)
        1       pc          small banana flower bud (sliced)
        1       bundle      pechay/bok choy
        1       bundle      string beans (cut into 2 inch slices)
        4       pcs         eggplants (sliced)
        1       cup         ground peanuts
        1/2     cup         peanut butter 
        1/2     cup         shrimp paste
        34      oz          water (1L)
        1/2     cup         annatto seeds (soaked in a cup of water)
        1/2     cup         toasted ground rice
        1       tbsp        garlic (minced)
        1       pc          onion (chopped)
                            salt and pepper

    ## Procedure
        
        1. In a large pot, bring the water to a boil.

        2. Put in the oxtail followed by the onions and simmer for 2.5 to 3 hrs or until tender (35 minutes if using a pressure cooker)

        3. Once the meat is tender, add the ground peanuts, peanut butter, and coloring (water from the annatto seed mixture) and simmer for 5 to 7 minutes
        
        4. Add the toasted ground rice and simmer for 5 minutes
        
        5. On a separate pan, saute the garlic then add the banana flower, eggplant, and string beans and cook for 5 minutes

        6. Transfer the cooked vegetables to the large pot (where the rest of the ingredients are)

        7. Add salt and pepper to taste
        
        8. Serve hot with shrimp paste. Enjoy!


# DONE 2. Mechado

    ## Description
        - Mechado is a beef dish originating from Spain. It is often served in the Philippines, a former colonial realm of Spain. Soy sauce and calamansi juice are often added to the marinating liquid. (Wikipedia)

    [Recipe Source](https://panlasangpinoy.com/filipino-pinoy-food-tomato-sauce-beef-mechado-recipe/#wprm-recipe-container-50803)

    ## Ingredients

        3       cloves      garlic (crushed)
        1       pc          large onion (sliced)
        2       lbs         beef chuck (cubed)
        8       oz          tomato sauce
        1       cup         water
        3       tbsp        cooking oil
        1       slice       lemon with rind
        1       piece       large potato (cubed)
        1/4     cup         soy sauce
        1/2     tsp         ground black pepper
        2       pcs         bay leaves (laurel)
                            salt to taste

    ## Procedure

        1. Heat cooking oil in a pan then saute the garlic and onion.

        2. Put-in the beef and saute for about 3 minutes or until color turns light brown

        3. Add the tomato sauce and water then simmer until the meat is tender. Add water as needed. Note this can take 60 to 120 minutes depending on the quality of the beef.
        
        4. Add the soy sauce, ground black pepper, lemon rind, laurel leaves, and salt then simmer until excess liquid evaporates

        5. Put-in the potatoes and cook until the potatoes are soft

        6. Place in a serving plate then serve hot with rice. Share and Enjoy!

# DONE 3. Chocolate Cake

    ## Description
    - Chocolate cake or chocolate gâteau (from French: gâteau au chocolat) is a cake flavored with melted chocolate, cocoa powder, or both. Chocolate cake is made with chocolate. It can also include other ingredients. These include fudge, vanilla creme, and other sweeteners. The history of chocolate cake goes back to 1764, when Dr. James Baker discovered how to make chocolate by grinding cocoa beans between two massive circular millstones. (Wikipedia)

    [Recipe Source](https://www.allrecipes.com/recipe/22953/dark-chocolate-cake-ii/)

    ## Ingredients
        2       cups        all-purpose flour
        2       cups        white sugar
        3/4     cup         unsweetened cocoa
        2       tsp         baking soda
        1       tsp         baking powder
        1/2     tsp         salt
        2                   eggs
        1       cup         cold brewed coffee
        1       cup         milk
        1/2     cup         vegetable oil
        2       tsp         vinegar

    ## Procedure

        1. Preheat oven to 350 degrees F (175 degrees C). Grease and flour a 9x13-inch pan.

        2. In a large bowl, combine the flour, sugar, cocoa, baking soda, baking powder and salt. Make a well in the center and pour in the eggs, coffee, milk, oil and vinegar. Mix until smooth; the batter will be thin. Pour the batter into the prepared pan.

        3. Bake in the preheated oven for 35 to 40 minutes, or until a toothpick inserted into the center of the cake comes out clean. Allow to cool.


# DONE 4. Bubble Tea

    ## Description
    - Bubble tea is a tea-based drink. Originating in Taichung, Taiwan in the early 1980s, it includes chewy tapioca balls or a wide range of other toppings. Ice-blended versions are frozen and put into a blender, resulting in a slushy consistency. There are many varieties of the drink with a wide range of flavors. The two most popular varieties are black pearl milk tea and green pearl milk tea. (Wikipedia)

    [Recipe Source](https://www.allrecipes.com/recipe/39391/very-popular-bubble-tea/)

    ## Ingredients

        1       tsp     White Sugar
        1/3     cup     pearl tapioca
        1       cup     brewed black tea
        2       tbsp    milk
        4       tsp     white sugar
        1       cup     ice cubes

    ## Procedure

        1. In a small saucepan, bring 2 cups water to a boil. Stir in 1 teaspoon sugar until it dissolves. Toss in the pearl tapioca. Cook for about 20 minutes. Rinse, drain, and refrigerate until chilled.

        2. Pour tea, milk, and 4 teaspoons sugar into a cocktail shaker. Stir until the sugar has dissolved and the milk is well mixed in. Add the ice cubes, and shake so the whole drink can get cold. Pour into a glass, and add tapioca.


# 5. Ratatouille (Confit Byaldi)

    ## Description
    - Confit byaldi is a variation on the traditional French dish ratatouille by French chef Michel Guérard. (Wikipedia)

    - The original ratatouille recipe had the vegetables fried before baking. Since at least 1976, some French chefs have prepared the ratatouille vegetables in thin slices instead of the traditional rough-cut. Michel Guérard, in his book founding cuisine minceur (1976),[3] recreated lighter versions of the traditional dishes of nouvelle cuisine. His recipe, Confit bayaldi, differed from ratatouille by not frying the vegetables, removing peppers and adding mushrooms. (Wikipedia)

    [Recipe Source](https://www.thespruceeats.com/confit-byaldi-recipe-1375538)

    ## Ingredients

        1       cup     piperade
        1               zucchini (medium) (thinly sliced)
        1               yellow squash (medium) (thinly sliced)
        2               tomatoes (medium) (thinly sliced)
        2       cloves  garlic (crushed, finely chopped)
        1       tbsp    fresh thyme (chopped)
        3       tbsp    balsamic vinaigrette
        1       dash    sea salt

    ## Procedure

        1. Preheat the oven to 300. Lightly grease a large round or oval baking dish.

        2. Spread the piperade along the bottom of the dish. Alternating ingredients, arrange the zucchini, yellow squash, and tomatoes in a spiral pattern around the dish. Sprinkle the squash and tomatoes with the chopped garlic and thyme.

        3. Cover the confit byaldi with a single layer of parchment paper cut to fit the inside of the dish and rest atop the ingredients. Bake the ratatouille for 1 hour and 45 minutes. Remove the parchment paper and continue baking for an additional 25 minutes, until the top is lightly browned.

        4. Drizzle the surface of the confit byaldi with balsamic vinaigrette and season it with salt and pepper. Serve the dish fresh from the oven or allow the flavors to develop and mellow in the refrigerator for a day.


# DONE 6. Doughnut

    ## Description
    - A doughnut or donut is a type of fried dough confection or dessert food. It is popular in many countries and is prepared in various forms as a sweet snack that can be homemade or purchased in bakeries, supermarkets, food stalls, and franchised specialty vendors. (Wikipedia)

    [Recipe Source](https://www.allrecipes.com/recipe/40054/glazed-yeast-doughnuts/)

    ## Ingredients

        3/4     cup     Scalded Milk
        1/3     cup     Granulated Sugar
        1/4     tbsp    Salt
        0.25    oz      active dry yeast
        1/4     cup     Warm water
        4       cups    Sifted all-purpose flour
        1/3     cup     butter/margarine
        2               eggs, beaten
        2       cups    confectioners' sugar
        6       tbsp    milk
                        oil, for deep frying
        1       tsp     freshly grated nutmeg (Optional)

    ## Procedure

        1. In a medium bowl, stir together the scalded milk, sugar, and salt. Set aside to cool until tepid. If using nutmeg, stir it into the flour, and add 2 cups of the mixture to the milk, and beat until well blended.

        2. In a small bowl, dissolve the yeast in warm water. Stir into the milk and flour mixture, then mix in the butter and eggs. Mix in the remaining flour 1/2 cup at a time. When dough is firm enough, turn it out onto a floured surface, and knead for 3 to 4 minutes. Place into an oiled bowl, cover and allow dough to rise until doubled in bulk. This should take 30 to 45 minutes.

        3. On a lightly floured surface, roll the dough out to 1/2 inch in thickness. Cut into circles using a donut cutter, or round cutter. Set aside to rise for 30 to 40 minutes, or until light.

        4. Heat one inch of oil in a deep heavy frying pan to 375 degrees F (190 degrees C). Fry donuts a few at a time. Cook on each side until golden brown, then remove to drain on paper towels. Glaze while warm, or just sprinkle with sugar.

        5. To make the glaze, stir together the confectioners' sugar and 6 tablespoons milk until smooth. Dip warm donuts into glaze, and set aside to cool.